---
title: "My Second Post"
date: 2018-04-20T21:35:14+02:00
draft: true
---

GITLAB GIT CODE:
When you create a new repo locally, instead of going to GitLab to to create the new project, all without leaving your terminal. 

If you have access to that namespace, we will automatically create a new project under that GitLab namespace with its visibility set to Private by default (you can later change it in the project's settings).
